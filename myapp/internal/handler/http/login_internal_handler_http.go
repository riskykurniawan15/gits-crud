package http

import (
	"myapp/entity"
	"net/http"

	"github.com/labstack/echo/v4"
)

// Login returns health check for the service.
func Login(echoCtx echo.Context) error {
	var res = entity.NewResponse(http.StatusOK, "Login Success!", nil)
	return echoCtx.JSON(res.Status, res)
}
