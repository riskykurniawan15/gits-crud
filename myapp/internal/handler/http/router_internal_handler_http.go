package http

import (
	custom "myapp/internal/handler/middleware"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// NewGinEngine creates an instance of echo.Engine.
// gin.Engine already implements net/http.Handler interface.

func NewGinEngine(accountHandler *AccountHandler, customerHandler *CustomerHandler, orderHandler *OrderHandler, productHandler *ProductHandler, orderdetailsHandler *Order_DetailsHandler, internalUsername, internalPassword string) *echo.Echo {
	engine := echo.New()

	engine.GET("/", Status)
	engine.GET("/health", Health)
	engine.GET("/version", Version)
	engine.POST("/register", accountHandler.CreateAccount)

	login := engine.Group("/login")
	login.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
		// Be careful to use constant time comparison to prevent timing attacks
		res := custom.Check_Auth(username, password)
		return res, nil
	}))
	login.POST("", Login)

	mid := engine.Group("/data")
	mid.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
		// Be careful to use constant time comparison to prevent timing attacks
		res := custom.Check_Auth(username, password)
		return res, nil
	}))

	mid.POST("/create-account", accountHandler.CreateAccount)
	mid.GET("/list-account", accountHandler.GetListAccount)
	mid.GET("/get-account/:id", accountHandler.GetDetailAccount)
	mid.PUT("/update-account/:id", accountHandler.UpdateAccount)
	mid.DELETE("/delete-account/:id", accountHandler.DeleteAccount)

	mid.POST("/create-customer", customerHandler.CreateCustomer)
	mid.GET("/list-customer", customerHandler.GetListCustomer)
	mid.GET("/get-customer/:id", customerHandler.GetDetailCustomer)
	mid.PUT("/update-customer/:id", customerHandler.UpdateCustomer)
	mid.DELETE("/delete-customer/:id", customerHandler.DeleteCustomer)

	mid.POST("/create-order", orderHandler.CreateOrder)
	mid.GET("/list-order", orderHandler.GetListOrder)
	mid.GET("/get-order/:id", orderHandler.GetDetailOrder)
	mid.PUT("/update-order/:id", orderHandler.UpdateOrder)
	mid.DELETE("/delete-order/:id", orderHandler.DeleteOrder)

	mid.POST("/create-product", productHandler.CreateProduct)
	mid.GET("/list-product", productHandler.GetListProduct)
	mid.GET("/get-product/:id", productHandler.GetDetailProduct)
	mid.PUT("/update-product/:id", productHandler.UpdateProduct)
	mid.DELETE("/delete-product/:id", productHandler.DeleteProduct)

	mid.POST("/create-orderdetails", orderdetailsHandler.CreateOrder_Details)
	mid.GET("/list-orderdetails", orderdetailsHandler.GetListOrder_Details)
	mid.GET("/get-orderdetails/:id", orderdetailsHandler.GetDetailOrder_Details)
	mid.PUT("/update-orderdetails/:id", orderdetailsHandler.UpdateOrder_Details)
	mid.DELETE("/delete-orderdetails/:id", orderdetailsHandler.DeleteOrder_Details)

	return engine
}
