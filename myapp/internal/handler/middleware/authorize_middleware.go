package middleware

import (
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Account struct {
	Username  string
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}

func Check_Auth(username, password string) bool {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	dsns := "host=" + os.Getenv("DATABASE_HOST") + " user=" + os.Getenv("DATABASE_USERNAME") + " password=" + os.Getenv("DATABASE_PASSWORD") + " dbname=" + os.Getenv("DATABASE_NAME") + " port=" + os.Getenv("DATABASE_PORT") + " sslmode=disable"
	dbs, _ := gorm.Open(postgres.Open(dsns), &gorm.Config{})

	p := Account{}
	dbs.Where("Username = ?", username).Table("account").First(&p)

	if err := bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(password)); err != nil {
		return false
	}
	return true
}
